#include <fstream>
#include <regex>

#include "DialogueParser.h"
#include "../ParserUtil.h"

#include <spdlog/spdlog.h>

const char* DialogueParser::TREE_ASSET_DIRECTORY = "assets/dialogue-trees/";

static std::string emptyLineRegex_start;
static std::string emptyLineRegex_middle;
static std::string emptyLineRegex_end;

static std::string blockContent_regex;
static std::string choices_regex;
static std::string choiceBlock_regex;

static std::string blockIdentifier_regex;
static std::string blockScope_regex;
static std::string block_regex;

static std::string firstDialogueLine_regex;
static std::string secondDialogueLine_regex;


DialogueParser::DialogueParser(){
    emptyLineRegex_start = fmt::format(R"((^{}\n))",zeroOrMoreIgnorableCharsExcludingNewLines_regex);
    emptyLineRegex_middle = fmt::format(R"((\n{}\n))",zeroOrMoreIgnorableCharsExcludingNewLines_regex);
    emptyLineRegex_end = fmt::format(R"((\n{}$))",zeroOrMoreIgnorableCharsExcludingNewLines_regex);

    blockContent_regex = R"((?:[^}]|\n|\r)*[^\h\n\t\r\s ])";
    choices_regex = fmt::format("({}) *=> *({})",dialogueSentence_regex,id_regex);
    choiceBlock_regex = fmt::format(R"(^{}\[{}({}){}\])",zeroOrMoreIgnorableChars_regex,zeroOrMoreIgnorableChars_regex,anything_regex,zeroOrMoreIgnorableChars_regex);

    blockIdentifier_regex = fmt::format(R"({}({}){})", zeroOrMoreIgnorableChars_regex,id_regex,zeroOrMoreIgnorableChars_regex);

    blockScope_regex = fmt::format(R"({}({}){})", zeroOrMoreIgnorableChars_regex,blockContent_regex,zeroOrMoreIgnorableChars_regex);
    //{{ and }} are double to escape for fmt
    block_regex = fmt::format(R"(\({}\){}\{{{}\}})", blockIdentifier_regex, zeroOrMoreIgnorableChars_regex, blockScope_regex);

    firstDialogueLine_regex = fmt::format(R"(^{}({}){}:{}({}){})", zeroOrMoreIgnorableChars_regex,id_regex,zeroOrMoreIgnorableChars_regex,zeroOrMoreIgnorableChars_regex,dialogueSentence_regex,newLine_regex);
    secondDialogueLine_regex = fmt::format(R"(^{}([^:\[\n\r]+){})", zeroOrMoreIgnorableChars_regex,newLine_regex);

}

DialogueParser::~DialogueParser() {

    for(const auto& pair : idDialogueBlockMap)
        delete pair.second;
}

void DialogueParser::initialize(const std::unordered_map<std::string, Character*>& idCharacterMap_){

  //TODO pull out into general assert
#ifdef DEBUG
  if(!idCharacterMap.empty()){
    throw std::runtime_error("Trying to initialize idCharacterMap when already initialized.");
  }
#endif
  this->idCharacterMap = idCharacterMap_;
}


DialogueBlock* DialogueParser::getBlockFromId(const std::string& blockId) {

      if(idDialogueBlockMap.count(blockId)<1){
          throw std::invalid_argument("Trying to retrieve a DialogueBlock with the non-existing id: "+blockId);
      }

    return idDialogueBlockMap[blockId];
}
void DialogueParser::createNewBlock(const std::string& blockId){

  //TODO pull out into general assert
  #ifdef DEBUG
    if(idDialogueBlockMap.count(blockId)>0){
        throw std::runtime_error("Trying to add a new DialogueBlock with the already reserved id: "+blockId);
    }
  #endif
    idDialogueBlockMap[blockId] = new DialogueBlock(blockId);
}

static inline std::string getFileContent(const std::string& fileName) {

    std::ifstream file(DialogueParser::TREE_ASSET_DIRECTORY + fileName);
    std::string content((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    return content;
}

static inline void dirtyMSVC_GCC_newline_compatibilityFix(std::string& block_content)
{
  //delete all leading new lines
  bool isFirstCharOfContentNewLine = true;
  while (isFirstCharOfContentNewLine){
    if (block_content.c_str()[0] == '\n') {
      block_content.erase(0, 1);
    } else {
      isFirstCharOfContentNewLine = false;
    }
  }
}

void addExtraDialogueLines(std::string& block_content, std::vector<std::string>* lines){

    //TODO optimize, I don't need to do two seperate regex calls if I just use a regex with ^ to specify the next line, this is possible now that I preparse empty lines and fixed the extractSpeechNode bug (added newline to end)

    std::regex r_secondLine(secondDialogueLine_regex);
    std::smatch extraDialogueLines_matches;

    bool nextLineSpokenBySameCharacter = true;
    std::smatch sameCharacterLine_matches;

    while(nextLineSpokenBySameCharacter){

      //TODO find out what specific regex implementation difference is causing the deletion and non-deletion of the newlines that causes the problem
      dirtyMSVC_GCC_newline_compatibilityFix(block_content);

      std::string nextline = block_content.substr (0,block_content.find('\n'));
      nextLineSpokenBySameCharacter = std::regex_search(nextline, sameCharacterLine_matches, r_secondLine);

      if(nextLineSpokenBySameCharacter){
        std::regex_search(block_content, extraDialogueLines_matches, r_secondLine);
        lines->push_back(extraDialogueLines_matches.str(1));
        block_content = extraDialogueLines_matches.suffix().str();
      }
    }
}

std::vector<SpeechNode> DialogueParser::extractSpeechNodes(std::string& block_content) {

    std::vector<SpeechNode> speechNodes = {};

    std::regex r_line(firstDialogueLine_regex);
    std::smatch matches;

    while(std::regex_search(block_content, matches, r_line)){

        std::string character_id = matches.str(1);
        std::vector<std::string> lines = { matches.str(2) };
        block_content = matches.suffix().str();

        addExtraDialogueLines(block_content,&lines);

        speechNodes.emplace_back(SpeechNode(idCharacterMap[character_id], lines));
    }

    return speechNodes;
}

std::vector<Choice> DialogueParser::extractChoices(std::string& choicesBlock) {

    std::regex r_choice(choices_regex);
    std::smatch matches;

    std::vector<Choice> choices = {};
    while(std::regex_search(choicesBlock, matches, r_choice)){

        std::string text = matches.str(1);
        std::string goToBlockId = matches.str(2);

        Choice choice = {text, getBlockFromId(goToBlockId)};
        choices.emplace_back(choice);

        choicesBlock = matches.suffix().str();
    }
    return choices;
}

ChoiceNode DialogueParser::extractChoiceNode(std::string& block_content, bool& containedChoiceNode) {

    std::regex r_choiceBlock(choiceBlock_regex);
    std::smatch matches;

    containedChoiceNode = false;
    if(std::regex_search(block_content, matches, r_choiceBlock)){

        containedChoiceNode = true;
        std::string choicesBlock = matches.str(1);
        block_content = matches.suffix().str();

        std::vector<Choice> choices = extractChoices(choicesBlock);
        return ChoiceNode(choices);
    }
    return ChoiceNode({});
}

void DialogueParser::ParseDialogueBlock(const std::string& block_id, std::string block_content) {

    DialogueBlock* dialogueBlock = getBlockFromId(block_id);

    std::vector<SpeechNode> speechNodes = extractSpeechNodes(block_content);
    for(auto& speechNode : speechNodes) {
       dialogueBlock->addSpeechNode(speechNode);
    }

    bool containedChoiceNode = false;
    ChoiceNode choiceNode = extractChoiceNode(block_content,containedChoiceNode);
    if(containedChoiceNode){
        dialogueBlock->addChoiceNode(choiceNode);
    }
}

static void inline removeComments(std::string& resource){

  std::regex r_comment(comment_regex);
  std::smatch matches;

  while(std::regex_search(resource, matches, r_comment)){

    resource = matches.suffix().str();
  }
}

//TODO add unit test for start, middle and end
static void inline removeEmptyLines(std::string& resource){

  //remove emptylines at start of file
  std::regex r_emptyLine_start(emptyLineRegex_start);
  resource = std::regex_replace (resource, r_emptyLine_start,"");

  //remove emptylines in middle of file
  std::regex r_emptyLine_middle(emptyLineRegex_middle);
  resource = std::regex_replace (resource, r_emptyLine_middle,"\n");

  //remove emptylines at end of file
  std::regex r_emptyLine_end(emptyLineRegex_start);
  resource = std::regex_replace (resource, r_emptyLine_end,"");



}

void DialogueParser::PreParse(std::string resource) {

    removeComments(resource);
    removeEmptyLines(resource);

    std::regex r_blocks(block_regex);
    std::smatch matches;

    while(std::regex_search(resource, matches, r_blocks)){

        std::string block_id = matches.str(1);
        createNewBlock(block_id);
        resource = matches.suffix().str();
    }
}

void DialogueParser::parse(const std::string& fileName) {

    std::string resource = getFileContent(fileName);

    PreParse(resource);

    std::regex r_blocks(block_regex);
    std::smatch matches;

    while(std::regex_search(resource, matches, r_blocks)){

        std::string block_id = matches.str(1);
        std::string block_content = matches.str(2);

        ParseDialogueBlock(block_id,block_content);
        resource = matches.suffix().str();
    }
}

const std::unordered_map<std::string, DialogueBlock *> &DialogueParser::getIdDialogueBlockMap() const {
    return idDialogueBlockMap;
}

