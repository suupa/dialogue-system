#pragma once

#include "../../dialogue/DialogueBlock.h"

class DialogueParser {

private:
    std::unordered_map<std::string,DialogueBlock*> idDialogueBlockMap = {};
    std::unordered_map<std::string, Character*> idCharacterMap = {};

    void PreParse(std::string resource);
    void ParseDialogueBlock(const std::string &block_id, std::string block_content);
    std::vector<SpeechNode> extractSpeechNodes(std::string &block_content);
    ChoiceNode extractChoiceNode(std::string &block_content, bool &containedChoiceNode);
    std::vector<Choice> extractChoices(std::string &choicesBlock);

    void createNewBlock(const std::string &blockId);
public:
    static const char* TREE_ASSET_DIRECTORY;

    DialogueParser();
    ~DialogueParser();
    void initialize(const std::unordered_map<std::string, Character *>& idCharacterMap);
    void parse(const std::string& fileName);
    DialogueBlock* getBlockFromId(const std::string &blockId);
    const std::unordered_map<std::string,DialogueBlock*>& getIdDialogueBlockMap() const;

};


