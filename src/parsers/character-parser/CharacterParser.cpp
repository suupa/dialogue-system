#include <fstream>
#include <regex>
#include <iostream>
#include "CharacterParser.h"

const char* CharacterParser::CHARACTER_ASSET_DIRECTORY = "assets/character-data/";

static std::string character_regex;

CharacterParser::CharacterParser() {

  character_regex = R"((\w*): ?(\w*))";
}

CharacterParser::~CharacterParser() {

    for(const auto& pair : idCharacterMap)
        delete pair.second;
}

static inline std::string getFileContent() {

  std::stringstream ss;
  ss << CharacterParser::CHARACTER_ASSET_DIRECTORY << "characters.txt";
  std::ifstream file(ss.str());
  std::string content((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

  return content;
}

void CharacterParser::parse() {

  std::string charactersResource = getFileContent();

  std::regex r_character(character_regex);
  std::smatch matches;

  while(std::regex_search(charactersResource, matches, r_character)){

    idCharacterMap[matches.str(1)] = new Character(matches.str(1), matches.str(2));
    charactersResource = matches.suffix().str();
  }
}

const std::unordered_map<std::string, Character*>& CharacterParser::getIdDialogueBlockMap() const {
    return idCharacterMap;
}