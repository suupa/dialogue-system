#pragma once
#include <string>
#include "../../character/Character.h"

class CharacterParser {
private:
    std::unordered_map<std::string,Character*> idCharacterMap = {};
public:
    static const char* CHARACTER_ASSET_DIRECTORY;

    CharacterParser();
    ~CharacterParser();
    void parse();

    const std::unordered_map<std::string, Character *>& getIdDialogueBlockMap() const;
};


