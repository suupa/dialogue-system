#pragma once
#include <spdlog/spdlog.h>

static const std::string zeroOrMoreIgnorableChars_regex = R"([\h\n\t\r\s ]*)";
static const std::string zeroOrMoreIgnorableCharsExcludingNewLines_regex = R"([\h\t\s ]*)";
static const std::string newLine_regex = R"((?:[\n\r]|$))";
static const std::string oneOrMoreNewLine_regex = R"([\n\r]*)";
static const std::string anything_regex = R"((?:.|\n|\r)*)";

static const std::string id_regex = R"([A-Z_]*)";

static const std::string startOfDialogueSentence_regex = R"([A-Za-z0-9.(])";
static const std::string anythingExceptNewline_regex = R"(.*)";
static const std::string endOfDialogueSentence_regex = R"([A-Za-z0-9.?!)])";

static const std::string dialogueSentence_regex = startOfDialogueSentence_regex + anythingExceptNewline_regex + endOfDialogueSentence_regex;

static const std::string comment_regex =  fmt::format(R"((?:(\/\/.*$)|(\/\*{}?\*\/)))",anything_regex);