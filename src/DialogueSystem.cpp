#include <spdlog/spdlog.h>

#include "DialogueSystem.h"

DialogueSystem::DialogueSystem()
: characterParser(CharacterParser()), dialogueParser(DialogueParser())
{
  characterParser.parse();
  const std::unordered_map<std::string, Character*> idCharacterMap = characterParser.getIdDialogueBlockMap();
  dialogueParser.initialize(idCharacterMap);
}

void DialogueSystem::addTree(const std::string& treePath)
{
  dialogueParser.parse(treePath);
}

void DialogueSystem::addAllTrees()
{
  for (auto& entry : std::filesystem::directory_iterator(DialogueParser::TREE_ASSET_DIRECTORY)) {
    dialogueParser.parse(entry.path().filename().string());
  }
}

void DialogueSystem::trigger(const std::string& blockId)
{
  try {
    DialogueBlock* dialogueBlock = dialogueParser.getBlockFromId(blockId);
    runBlock(dialogueBlock);
  } catch (std::invalid_argument& e) {

    fmt::print(
R"(The id you entered ("{}") is not recognized as a Dialogue Block id.
With the tree assets you are using your options for block ids are:
)",blockId);

    auto idBlockMap = dialogueParser.getIdDialogueBlockMap();
    for ( auto it = idBlockMap.begin(); it != idBlockMap.end(); ){
      fmt::print( "{}",it->first);

      it++;
      if((it) != idBlockMap.end()){
        fmt::print( ", ");
      }
    }
    fmt::print("\n");
  }
}
void DialogueSystem::runBlock(DialogueBlock *dialogueBlock)
{

  std::optional<std::string> nextBlockId;
  while(dialogueBlock->hasNextNode()){
    auto node = dialogueBlock->getNextNode();
    nextBlockId = node->run();
  }

  if(nextBlockId.has_value()){

    std::string& blockId = nextBlockId.value();
    DialogueBlock* dialogueBlock = dialogueParser.getBlockFromId(blockId);
    runBlock(dialogueBlock);
  }
}

#ifdef DEBUG
DialogueParser &DialogueSystem::getDialogueParser()
{
  return dialogueParser;;
}
#endif
