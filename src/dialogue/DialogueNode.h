#pragma once

#include "../character/Character.h"
#include <optional>

class DialogueNode {
public:
    virtual ~DialogueNode();
    virtual std::optional<std::string> run() = 0;
#ifdef DEBUG
    virtual std::string toTestString() = 0;
#endif
};
