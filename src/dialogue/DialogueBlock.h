#pragma once

#include <variant>
#include <memory>
#include <vector>
#include "DialogueNode.h"
#include "SpeechNode.h"
#include "ChoiceNode.h"


class DialogueBlock {
private:
   std::vector<std::unique_ptr<DialogueNode>> nodes = {};
   long unsigned int nodeIndex = 0;
public:
    explicit DialogueBlock(std::string id);

    ~DialogueBlock();

    void addSpeechNode(SpeechNode speechNode);
    void addChoiceNode(ChoiceNode choiceNode);

    const std::string id;
    DialogueNode* getNextNode();
    bool hasNextNode();

    #ifdef DEBUG
    std::string toTestString();
    #endif
};

