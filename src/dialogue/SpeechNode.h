#pragma once

#include "DialogueNode.h"

class SpeechNode : public DialogueNode {

private:
    Character* actor;
    std::vector<std::string> lines;
public:
    static const char* CONTINUE_INSTRUCTION;

    SpeechNode(Character* actor, std::vector<std::string> lines);
    std::optional<std::string> run() override;

    #ifdef DEBUG
        std::string toTestString() override;
    #endif

};


