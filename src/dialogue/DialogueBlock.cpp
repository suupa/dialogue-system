#include "DialogueBlock.h"

#include <utility>

DialogueBlock::DialogueBlock(std::string id)
    : id(std::move(id))
{

}

DialogueBlock::~DialogueBlock() {
}

bool DialogueBlock::hasNextNode() {
    return (nodeIndex < nodes.size());
}

DialogueNode *DialogueBlock::getNextNode() {
    return nodes[nodeIndex++].get();
}

void DialogueBlock::addSpeechNode(SpeechNode speechNode) {

    nodes.emplace_back(std::make_unique<SpeechNode>(std::move(speechNode)));
}

void DialogueBlock::addChoiceNode(ChoiceNode choiceNode) {

    nodes.emplace_back(std::make_unique<ChoiceNode>(std::move(choiceNode)));
}

#ifdef DEBUG
std::string DialogueBlock::toTestString() {

    std::string str = "("+id+"){\n";
    for(auto& node : nodes){
        str=str+ '\t' + node->toTestString();
    }
    str=str+"}";
    return str;
}
#endif