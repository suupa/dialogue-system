#include <spdlog/spdlog.h>

#include "ChoiceNode.h"
#include "DialogueBlock.h"

const char*  ChoiceNode::CHOICE_INSTRUCTION = "\t|\tPress the number corresponding to your choice...";
const char*  ChoiceNode::CHOICE_INPUT_INVALID_FEEDBACK = "That's not a valid choice. Your options are: {}";

ChoiceNode::ChoiceNode(std::vector<Choice> choices)
: choices(std::move(choices)){}

#ifdef DEBUG 
std::string ChoiceNode::toTestString() {

    std::string str = "[\n";
    for(const auto& choice : choices){

        str+= "\t\t" + choice.text +" => "+ choice.gotoBlock->id +'\n';
    }
    str+= "\t]\n";
    return str;
}
#endif

static inline void printBadInputFeedback(size_t maxIndex){

  std::stringstream options;
  for(unsigned int i=0;i<=maxIndex;i++){
    options << i;
    if(i==maxIndex-1){
      options << " or ";
    } else if(i!=maxIndex){
      options << ",";
    }
  }
  fmt::print(ChoiceNode::CHOICE_INPUT_INVALID_FEEDBACK,options.str());
  std::cout << '\n';
}

static inline unsigned int getChoiceIndexInput(size_t maxIndex) {

  char choiceIndexInput;

  while(true){
    std::cin >> choiceIndexInput;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    bool isvalid = true;
    unsigned int choiceIndex;
    switch(choiceIndexInput){
    case '0':
      choiceIndex=0; break;
    case '1':
      choiceIndex=1; break;
    case '2':
      choiceIndex=2; break;
    case '3':
      choiceIndex=3; break;
    case '4':
      choiceIndex=4; break;
    case '5':
      choiceIndex=5; break;
    case '6':
      choiceIndex=6; break;
    case '7':
      choiceIndex=7; break;
    case '8':
      choiceIndex=8; break;
    case '9':
      choiceIndex=9; break;
    default:
      isvalid = false;
      printBadInputFeedback(maxIndex);
    }

    if(isvalid){
      if(choiceIndex > maxIndex){
        printBadInputFeedback(maxIndex);
      } else {
        return choiceIndex;
      }
    }
  }
}

std::optional<std::string> ChoiceNode::run()
{
  std::cout << '[';
  for(size_t i=0;i<choices.size();i++) {
    fmt::print("{}: {}",i,choices[i].text);
    if(i != choices.size()-1){
      std::cout << " | ";
    }
  }
  std::cout << "]" << ChoiceNode::CHOICE_INSTRUCTION << '\n';

  unsigned long choiceIndex = getChoiceIndexInput(choices.size()-1);

  return choices[choiceIndex].gotoBlock->id;
}
