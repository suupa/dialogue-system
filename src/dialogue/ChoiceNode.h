#pragma once

#include <array>
#include "DialogueNode.h"

class DialogueBlock;

struct Choice {

    std::string text;
    DialogueBlock* gotoBlock;
};

class ChoiceNode : public DialogueNode {

private:
    std::vector<Choice> choices;

public:
    static const char* CHOICE_INSTRUCTION;
    static const char* CHOICE_INPUT_INVALID_FEEDBACK;

    explicit ChoiceNode(std::vector<Choice> choices);
    std::optional<std::string> run() override;
#ifdef DEBUG
    std::string toTestString() override;
#endif
};