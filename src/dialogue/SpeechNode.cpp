#include <spdlog/spdlog.h>
#include <stdexcept>

#include "SpeechNode.h"

const char*  SpeechNode::CONTINUE_INSTRUCTION = "\t|\tPress ENTER to continue...";

SpeechNode::SpeechNode(Character* actor, std::vector<std::string> lines)
    : actor(actor), lines(lines) {

    //TODO pull all these out in generic asserts
    #ifdef DEBUG
        if(lines.size() < 1) {throw std::invalid_argument("SpeechNode needs at least one line");}
    #endif
}

#ifdef DEBUG
std::string SpeechNode::toTestString() {
    std::string str = std::string(actor->name) + ": " + lines[0];
    for(size_t i=1;i<lines.size();i++) {
        str+= " | " + lines[i];
    }
    str+= '\n';

    return str;
}
#endif

std::optional<std::string> SpeechNode::run()
{
  for(size_t i=0;i<lines.size();i++) {
    fmt::print("{} : {}{}",actor->name,lines[i],SpeechNode::CONTINUE_INSTRUCTION);
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }
  return {};
}

