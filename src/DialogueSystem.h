#pragma once

#include "parsers/character-parser/CharacterParser.h"
#include "parsers/dialogue-parser/DialogueParser.h"

class DialogueSystem
{
private:
  CharacterParser characterParser;
  DialogueParser dialogueParser;

  void runBlock(DialogueBlock* dialogueBlock);
public:
  DialogueSystem();
  void addTree(const std::string& treePath);
  void addAllTrees();
  void trigger(const std::string& blockId);

#ifdef DEBUG
  DialogueParser& getDialogueParser();
#endif
};
