#include <docopt/docopt.h>
#include <spdlog/spdlog.h>

#include "parsers/character-parser/CharacterParser.h"
#include "parsers/dialogue-parser/DialogueParser.h"
#include "DialogueSystem.h"

static const std::string appName = PROJECT_NAME ;

static const std::string USAGE = fmt::format(
  R"({}.

    Usage:
          dialogue-system trigger <id> [--tree=<file>]
          dialogue-system (-h | --help)
          dialogue-system --version

    Options:
            -h --help     Show this screen.
            --version     Show version.
            --tree=<file>  Choose which Dialogue Tree to load (default=all available)

    Example:
          dialogue-system trigger BOB_AND_BART_NOTICE_YOU
  )", appName);

int main(int argc, const char **argv)
{
  std::map<std::string, docopt::value> args = docopt::docopt(USAGE,
    { std::next(argv), std::next(argv, argc) },
    true,// show help if requested
    appName+' '+PROJECT_VERSION );

  DialogueSystem dialogueSystem = DialogueSystem();

  if(args["--tree"].isString()){
    std::string tree = args["--tree"].asString();
    dialogueSystem.addTree(tree);
  } else {
    dialogueSystem.addAllTrees();
  }

  dialogueSystem.trigger(args["<id>"].asString());

  /*
  //TODO add logging to some useful places, add some log file and write to that
  //Use the default logger (stdout, multi-threaded, colored)
  spdlog::info("Hello, {}!", "World");
  */

}

