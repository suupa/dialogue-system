#pragma once

#include <unordered_map>

//small stub class
class Character {
public:
    Character(std::string id,std::string name);
    const std::string id;
    const std::string name;
};
