#include <catch2/catch.hpp>
#include <spdlog/spdlog.h>//TODO move this into precompiled header along with everywhere else

#include "../../src/parsers/character-parser/CharacterParser.h"
#include "../../src/parsers/dialogue-parser/DialogueParser.h"
#include "../../src/DialogueSystem.h"

#ifdef DEBUG
TEST_CASE("Parse Basic Tree File", "[TreeParsing]")
{
  std::string tree = "interrupting_bob_and_bart.tree";

  DialogueSystem dialogueSystem = DialogueSystem();
  dialogueSystem.addTree(tree);
  auto idBlockMap = dialogueSystem.getDialogueParser().getIdDialogueBlockMap();

  auto BOB_AND_BART_NOTICE_YOU = idBlockMap["BOB_AND_BART_NOTICE_YOU"];
  std::string expected_BOB_AND_BART_NOTICE_YOU_output =
R"((BOB_AND_BART_NOTICE_YOU){
	Bob: Hello Bart!
	Bart: Hello Bob! | Where have you been all day?
	Bob: I was at home.
	Bart: Oh, ok.. | Hey, see that guy there.. | Who is that?
	Bob: No idea.. | Hey, you there, what's your name?
	[
		(stay silent) => BOB_JUST_TRYING_TO_BE_NICE
		None of your business! => BOB_JUST_TRYING_TO_BE_NICE
		Hey, my name is Billy. => BOB_AND_BART_INTRODUCE_THEMSELVES
	]
})";
  REQUIRE(BOB_AND_BART_NOTICE_YOU->toTestString() == expected_BOB_AND_BART_NOTICE_YOU_output);

  auto BOB_JUST_TRYING_TO_BE_NICE = idBlockMap["BOB_JUST_TRYING_TO_BE_NICE"];
  std::string expected_BOB_JUST_TRYING_TO_BE_NICE_output =
R"((BOB_JUST_TRYING_TO_BE_NICE){
	Bob: Jesus.. | I was just trying to be nice.
})";
  REQUIRE(BOB_JUST_TRYING_TO_BE_NICE->toTestString() == expected_BOB_JUST_TRYING_TO_BE_NICE_output);

  auto BOB_AND_BART_INTRODUCE_THEMSELVES = idBlockMap["BOB_AND_BART_INTRODUCE_THEMSELVES"];
  std::string expected_BOB_AND_BART_INTRODUCE_THEMSELVES_output =
R"((BOB_AND_BART_INTRODUCE_THEMSELVES){
	Bob: Nice to meet you Billy, my name is Bob.
	Bart: Hey, my name is Bart.
})";
  REQUIRE(BOB_AND_BART_INTRODUCE_THEMSELVES->toTestString() == expected_BOB_AND_BART_INTRODUCE_THEMSELVES_output);

}
#endif

//TODO add empty tree file test (that it doesnt crash)